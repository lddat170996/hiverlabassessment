﻿using DG.Tweening;
using MEC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour
{
    public static GameMaster instance;

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        StartLoadGame();
    }

    #region Load Game

    public void StartLoadGame()
    {
        Timing.RunCoroutine(StartLoadGameCoroutine(OnLoadingPercentChanged));
    }

    private IEnumerator<float> StartLoadGameCoroutine(Action<float> loadingPercent)
    {
        short currentPercent = 0;
        bool waitTask = false;
        TopLevelCanvas.instance.Initialize();
        yield return Timing.WaitForOneFrame;
        ResourceManager.instance.InitializeAsync(null);
        PUNConnect.instance.Initialize();

        currentPercent = 10;
        loadingPercent?.Invoke(currentPercent);

        waitTask = true;
        LoadScene(GameConstant.MENU_SCENE, (complete) =>
        {
            DOTween.To(() => currentPercent, x =>
             {
                 loadingPercent?.Invoke(x);
             }, 100, 0.5f).SetEase(Ease.InOutSine).OnComplete(() =>
             {
                 TopLevelCanvas.instance._hudGameLoading.Hide();
                 FinishLoadingProcess();
             });


        });
        while (waitTask)
        {
            yield return Timing.WaitForOneFrame;
        }

        yield break;
    }

    private void OnLoadingPercentChanged(float curPercent)
    {
        TopLevelCanvas.instance._hudGameLoading.SetLoadingProgress(curPercent);
    }

    private void FinishLoadingProcess()
    {
        AudioSystem.instance.PlayBGM(BGM.BGM_MENU);
    }

    #endregion

    #region Scene Utils

    public void LoadScene(string sceneName, Action<bool> callback)
    {
        StartCoroutine(LoadSceneAsync(sceneName, callback));
    }

    IEnumerator LoadSceneAsync(string sceneName, Action<bool> callback)
    {
        yield return null;

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
        Debug.Log("Pro :" + asyncOperation.progress);
        //When the load is still in progress, output the Text and progress bar
        while (!asyncOperation.isDone)
        {
            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {
                asyncOperation.allowSceneActivation = true;
            }

            yield return null;
        }

        if (asyncOperation.isDone)
            callback?.Invoke(true);
    }

    #endregion

    #region PUN networking

    public void PUNCreateRoom()
    {
        PUNConnect.instance.CreateRoom("Room_01");
    }

    #endregion
}
