﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopLevelCanvas : BaseParentHUD
{
    public static TopLevelCanvas instance;

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this.gameObject);

    }

    public HUDGameLoading _hudGameLoading;

    public override void Initialize()
    {
        base.Initialize();
        _hudGameLoading.PreInit(EnumHUD.HUD_GAME_LOADING, null);
    }

}
