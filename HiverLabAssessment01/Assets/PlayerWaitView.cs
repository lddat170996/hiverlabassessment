﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerWaitView : MonoBehaviour
{
    public Image _imgAvatar;
    public TextMeshProUGUI _txtName;

    private Player Data;
    public void Initialize(Player user)
    {
        this.Data = user;
        _txtName.text = Data.NickName;
    }
}
