﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

public class HUDRoomWaiting : BaseHUD
{
    public Transform _rectPlayers;
    public Button _btnStartGame;
    public PlayerWaitView _playerWaitViewPrefab;

    private List<Player> _listPlayers;
    private List<PlayerWaitView> _listPlayerViews;

    public override void PreInit(EnumHUD type, IParentHud _parent, params object[] args)
    {
        base.PreInit(type, _parent, args);
        _btnStartGame.interactable = false;
        _listPlayers = new List<Player>();
        _listPlayerViews = new List<PlayerWaitView>();
        if (!isInit)
        {
            PUNConnect.instance.HandlePlayerEnteredRoom += OnNewPlayer;
            isInit = true;

            foreach (var player in PUNConnect.instance.CurrentJoinedRoom.Players)
            {
                CreateNewPlayerView(player.Value);
            }

        }

        ResetLayout();
    }

    private void OnDestroy()
    {
        PUNConnect.instance.HandlePlayerEnteredRoom -= OnNewPlayer;
    }

    private void OnNewPlayer(Player player)
    {
        CreateNewPlayerView(player);
    }

    private void CreateNewPlayerView(Player data)
    {
        if (_listPlayers.FirstOrDefault(x => x.UserId == data.UserId) == null)
        {
            _listPlayers.Add(data);

            PlayerWaitView view = GameObject.Instantiate(_playerWaitViewPrefab, _rectPlayers);
            view.transform.localScale = Vector3.one;
            view.transform.localPosition = Vector3.zero;
            view.Initialize(data);
            _listPlayerViews.Add(view);
        }
        ResetLayout();
    }

    public void ResetLayout()
    {
        _btnStartGame.gameObject.SetActiveIfNot(PUNConnect.instance.IsMasterClient);
        if (PUNConnect.instance.CurrentJoinedRoom.PlayerCount >= GameConstant.MINIMUM_START_GAME)
        {
            _btnStartGame.interactable = true;
        }
    }

    public void OnButtonStartGame()
    {
        PUNConnect.instance.StartGame();
    }
}
