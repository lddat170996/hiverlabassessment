﻿using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public enum PUNState
{
    NONE,
    JOINED_SERVER,
    JOINED_LOBBY,
    JOINED_ROOM,
    PLAYING_GAME
}

public class PUNConnect : MonoBehaviourPunCallbacks
{
    public static PUNConnect instance;
    public PUNState PunState { get; protected set; }

    public bool IsMasterClient {
        get { return PhotonNetwork.IsMasterClient; }
    }

    public Player LocalPlayer {
        get {
            return PhotonNetwork.LocalPlayer;
        }
    }


    //Actions callback
    public Action HandleJoinedLobby;
    public Action<Player> HandlePlayerEnteredRoom;
    public Action<bool> HandleJoinedRoom;

    private void Awake()
    {
        instance = this;
    }

    public void Initialize()
    {
        PhotonNetwork.GameVersion = "0.0.1";
        PhotonNetwork.NickName = "Default-Player";
        PhotonNetwork.ConnectUsingSettings();
        PhotonNetwork.AutomaticallySyncScene = true;

        this.PunState = PUNState.NONE;
    }

    public void SetNickName(string nickName)
    {
        PhotonNetwork.NickName = nickName;
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        Debug.Log("[PUN] OnConnectedToMaster");
        this.PunState = PUNState.JOINED_SERVER;
        if (!PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinLobby();
        }
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);

        Debug.Log($"[PUN] OnDisconnected {cause.ToString()}");
    }

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        Debug.Log("[PUN] OnJoinedLobby");
        this.PunState = PUNState.JOINED_LOBBY;

    }

    #region Join or Create Room
    public Action<List<RoomInfo>> _OnRoomListUpdated;

    public Room CurrentJoinedRoom {
        get { return PhotonNetwork.CurrentRoom; }
    }

    public void JoinRandomRoom()
    {
        Debug.Log("[PUN] JoinRandomRoom");
        PhotonNetwork.JoinRandomRoom();
    }

    public void CreateRoom(string roomName)
    {
        if (!PhotonNetwork.IsConnected)
            return;

        RoomOptions options = new RoomOptions();
        options.MaxPlayers = 4;
        options.BroadcastPropsChangeToAll = true;
        PhotonNetwork.JoinOrCreateRoom(roomName, options, TypedLobby.Default);

    }

    public void LeaveRoom()
    {
        if (!PhotonNetwork.IsConnected || !PhotonNetwork.InRoom)
            return;
        Debug.Log("[PUN] Leave Room");
        PhotonNetwork.LeaveRoom();
    }

    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        Debug.Log("[PUN] OnCreatedRoom");
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        Debug.LogError($"[PUN] OnCreateRoomFailed code:{returnCode} msg:{message}");
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);
        Debug.Log($"[PUN] OnRoomListUpdate {roomList.Count}");
        _OnRoomListUpdated?.Invoke(roomList);
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log($"[PUN] OnJoinedRoom {PhotonNetwork.CurrentRoom.Name}");
        this.PunState = PUNState.JOINED_ROOM;
        HandleJoinedRoom?.Invoke(true);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);
        Debug.LogError($"[PUN] OnJoinRoomFailed {returnCode} {message} ");
        HandleJoinedRoom?.Invoke(false);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);
        Debug.LogError($"[PUN] OnJoinRandomFailed {returnCode} {message}");
        if (returnCode == 32760)
        {
            CreateRoom(FBUtils.RandomString(10));
        }
        HandleJoinedRoom?.Invoke(false);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log("[PUN] OnPlayerEnteredRoom");
        HandlePlayerEnteredRoom?.Invoke(newPlayer);


    }
    #endregion

    #region Game Play
    public GameObject SpawnObjetOverNetwork(string path, Vector3 position, Quaternion rotation)
    {
        return PhotonNetwork.Instantiate(Path.Combine("PUNPrefabs", path), position, rotation);
    }

    public void DestroyNetworkObject(GameObject go)
    {
        PhotonNetwork.Destroy(go);
    }
    #endregion

    public void StartGame()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;

        PhotonNetwork.CurrentRoom.IsVisible = false;
        PhotonNetwork.CurrentRoom.IsOpen = false;

        PhotonNetwork.LoadLevel(GameConstant.GAME_SCENE_01);
    }
}
