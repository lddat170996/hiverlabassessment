﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreView : MonoBehaviour
{
    public TextMeshProUGUI _txtName;
    public TextMeshProUGUI _txtScore;

    public void Initialize(string name)
    {
        this.transform.localScale = Vector3.one;
        _txtName.text = name;
        _txtScore.text = "0";
    }

    public void SetScore(int newScore)
    {
        _txtScore.text = newScore.ToString();
    }
}
