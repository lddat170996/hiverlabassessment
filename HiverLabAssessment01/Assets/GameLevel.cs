﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using YoutubePlayer;

public class GameLevel : MonoBehaviour
{
    public VideoPlayer _videoPlayer;

    [Header("SkyBox Colours")]
    public Color startColour;
    public Color endColour;

    private float step = 0f;

    private void Start()
    {
        _videoPlayer.PlayYoutubeVideoAsync("https://www.youtube.com/watch?v=twX6Q2c94BAv");
        step = 0f;
    }

    private void Update()
    {
        RenderSettings.skybox.SetColor("_Tint", Color.Lerp(startColour, endColour, step));
        step += Time.deltaTime / (GameConstant.GAME_DURATION / 2.0f);
    }

}
