﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class BGMDefine
{
    public BGM bgmEnum;
    public AudioClip bgm;
}

[Serializable]
public class SFXDefine
{
    public SFX sfxEnum;
    public AudioClip sfx;
}

public class ResourceManager : BaseSystem
{
    public static ResourceManager instance;
    private void Awake()
    {
        instance = this;
    }

    [Header("Resource Assets")]
    public ResourcesSO _resourcesAsset;

    [Header("BGM & SFX")]
    public List<BGMDefine> _listBGM;

    [Space(10)]
    public List<SFXDefine> _listSFX;

    public override IEnumerator<float> InitializeAsyncCoroutine(Action callback, params object[] args)
    {
        callback?.Invoke();
        yield break;
    }

    public AudioClip GetBGM(BGM bgm)
    {
        return _listBGM.FirstOrDefault(x => x.bgmEnum == bgm)?.bgm;
    }

    public AudioClip GetSFX(SFX sfx)
    {
        return _listSFX.FirstOrDefault(x => x.sfxEnum == sfx)?.sfx;
    }
}
