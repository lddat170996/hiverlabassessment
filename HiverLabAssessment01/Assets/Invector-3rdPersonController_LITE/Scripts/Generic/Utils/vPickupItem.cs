﻿using UnityEngine;
using System.Collections;

public class vPickupItem : MonoBehaviour
{
    AudioSource _audioSource;
    public AudioClip _audioClip;
    public GameObject _particle;

    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<NetworkPlayer>()?.PickCoin(+1);
           
            AudioSystem.instance.PlaySFX(SFX.PICK_COIN);
            PUNConnect.instance.DestroyNetworkObject(this.gameObject);
            GamePlayController.instance.pView.RPC("NetPickCoin", Photon.Pun.RpcTarget.AllBufferedViaServer);


        }
    }
}