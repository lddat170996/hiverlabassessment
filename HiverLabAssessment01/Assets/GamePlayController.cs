﻿using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum GameState
{
    NONE,
    WAITING,
    GAME_START,
    GAME_FINISHED
}

public class GamePlayController : BaseSystem
{
    public PhotonView pView { get; private set; }
    private int totalPlayerReady;
    public long EndGameTS { get; private set; }

    public int TotalCoins { get; private set; }

    public Transform _markerSpawnPlayer;

    public GameState _state { get; private set; }
    public static GamePlayController instance;

    public Transform _playerSpawnPoint;
    public Transform _coinParents;
    
    private float timerGame = 0f;
    private List<GameObject> _totalCoins = new List<GameObject>();
    private GameObject _localPlayer;

    public void SetGameState(GameState state)
    {
        _state = state;
        switch (_state)
        {
            case GameState.NONE:
                break;
            case GameState.WAITING:
                break;
            case GameState.GAME_START:
                break;
            case GameState.GAME_FINISHED:
                if (PUNConnect.instance.IsMasterClient)
                {
                    pView.RPC("NetFinishGame", RpcTarget.AllBufferedViaServer);
                }
                break;
            default:
                break;
        }
    }

    private void Awake()
    {
        instance = this;
        pView = this.GetComponent<PhotonView>();
    }

    private void Start()
    {
        InitializeAsync(null);
    }

    private void OnDestroy()
    {
        RemoveListeners();
    }

    private void AddListeners()
    {
        EventSystemServiceStatic.AddListener(this, EVENT_NAME.UPDATE_PLAYER_INFO, new Action(UpdatePlayersScore));
    }

    private void UpdatePlayersScore()
    {
        GamePlayCanvas.instance.UpdatePlayerScore();
    }

    private void RemoveListeners()
    {
        EventSystemServiceStatic.RemoveListener(this, EVENT_NAME.UPDATE_PLAYER_INFO, new Action(UpdatePlayersScore));
    }

    public override IEnumerator<float> InitializeAsyncCoroutine(Action callback, params object[] args)
    {
        AudioSystem.instance.PlayBGM(BGM.BGM_BATTLE);
        SetGameState(GameState.WAITING);
        totalPlayerReady = 0;
        AddListeners();
        GamePlayCanvas.instance.Initialize();
        SpawnPlayer();
        if (PUNConnect.instance.IsMasterClient)
        {
            SpawnCoins();
        }
        callback?.Invoke();
        Cursor.visible = false;
        yield break;
    }

    public void SpawnPlayer()
    {
        var randPos = _markerSpawnPlayer.GetRandPos();
        _localPlayer = PUNConnect.instance.SpawnObjetOverNetwork(GameConstant.PLAYER_CONTROLLER, randPos, Quaternion.identity);
    }

    public void SpawnCoins()
    {
        foreach (Transform marker in _coinParents)
        {
            var coin = PUNConnect.instance.SpawnObjetOverNetwork("NetworkCoin", marker.transform.position, Quaternion.identity);
            _totalCoins.Add(coin);
        }

        TotalCoins = _coinParents.childCount;
    }

    public void IncreasePlayerReady()
    {
        totalPlayerReady++;
        if (totalPlayerReady >= PUNConnect.instance.CurrentJoinedRoom.PlayerCount)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                var endGameTS = TimeService.GetCurrentTimeStamp() + GameConstant.GAME_DURATION;
                pView.RPC("NetStartGame", RpcTarget.AllBufferedViaServer, endGameTS);
            }
        }
    }

    [PunRPC]
    public void NetPickCoin()
    {
        if(PUNConnect.instance.IsMasterClient)
        {
            TotalCoins -= 1;
            if (TotalCoins <= 0)
            {
                SetGameState(GameState.GAME_FINISHED);
            }
        }
    }

    [PunRPC]
    public void NetStartGame(long endGameTS)
    {
        EndGameTS = endGameTS;
        foreach (var player in PUNConnect.instance.CurrentJoinedRoom.Players)
        {
            player.Value.SetState(PLAYER_STATE.PLAYING_BATTLE);
        }
        SetGameState(GameState.GAME_START);
        GamePlayCanvas.instance.HideHUD(EnumHUD.HUD_WINLOSE);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    [PunRPC]
    public void NetFinishGame()
    {
        foreach (var player in PUNConnect.instance.CurrentJoinedRoom.Players)
        {
            if (player.Value.IsMasterClient)
            {
                player.Value.SetState(PLAYER_STATE.READY);
            }
            else
                player.Value.SetState(PLAYER_STATE.FINISH_GAME);
        }
        CleanUp();
        GamePlayCanvas.instance.ShowHUD(EnumHUD.HUD_WINLOSE);
        EventSystemServiceStatic.DispatchAll(EVENT_NAME.UPDATE_PLAYER_INFO);
    }

    [PunRPC]
    public void NetReplay()
    {
        SetGameState(GameState.WAITING);
        totalPlayerReady = 0;

        foreach (var player in PUNConnect.instance.CurrentJoinedRoom.Players)
        {
            player.Value.SetState(PLAYER_STATE.PLAYING_BATTLE);
        }
        SpawnPlayer();
        if (PUNConnect.instance.IsMasterClient)
        {
            SpawnCoins();
            var endGameTS = TimeService.GetCurrentTimeStamp() + GameConstant.GAME_DURATION;
            pView.RPC("NetStartGame", RpcTarget.AllBufferedViaServer, endGameTS);
        }
    }

    [PunRPC]
    public void NetRefreshHUDFinish()
    {
        EventSystemServiceStatic.DispatchAll(EVENT_NAME.UPDATE_PLAYER_INFO);
    }

    public void CleanUp()
    {
        for (int i = _totalCoins.Count - 1; i >= 0; i--)
        {
            PUNConnect.instance.DestroyNetworkObject(_totalCoins[i]);
        }

        PUNConnect.instance.DestroyNetworkObject(_localPlayer);

        _totalCoins.Clear();
    }

    public Player GetWinner()
    {
        Player winner = null;
        var pair = PUNConnect.instance.CurrentJoinedRoom.Players.MaxBy(x => x.Value.GetScore());
        winner = pair.Value;
        return winner;
    }

    private void Update()
    {
        float _deltaTime = Time.deltaTime;
        if (_state == GameState.WAITING)
        {

        }
        else if (_state == GameState.GAME_START)
        {
            timerGame += _deltaTime;
            if (timerGame >= 1.0f)
            {
                timerGame = 0f;
                long remain = EndGameTS - TimeService.GetCurrentTimeStamp();
                remain = (long)Mathf.Clamp(remain, 0, long.MaxValue);
                GamePlayCanvas.instance.SetPlayTime(remain);

                if (remain <= 0)
                {
                    SetGameState(GameState.GAME_FINISHED);
                }
            }
        }
    }


}
