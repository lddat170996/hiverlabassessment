﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPlayerManager : MonoBehaviour
{
    private PhotonView pView;

    private void Awake()
    {
        pView = this.GetComponent<PhotonView>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (pView.IsMine)
        {
            var RandPos = GamePlayController.instance._playerSpawnPoint.position;
            RandPos.x += UnityEngine.Random.Range(-3, 3f);
            RandPos.z += UnityEngine.Random.Range(-3, 3f);

            PUNConnect.instance.SpawnObjetOverNetwork(GameConstant.PLAYER_CONTROLLER, RandPos, Quaternion.identity);
        }
    }


}
