﻿using Ez.Pooly;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class GamePlayCanvas : BaseParentHUD
{
    public static GamePlayCanvas instance;
    private void Awake()
    {
        instance = this;
    }

    public Transform _rectGameInfo;
    public TextMeshProUGUI _txtTimeLeft;

    [Header("Other refs")]
    public ScoreView _scoreViewPrefab;

    private Dictionary<int, ScoreView> _dictScoreView;

    public override void Initialize()
    {
        base.Initialize();
        _dictScoreView = new Dictionary<int, ScoreView>();
        foreach (var player in PUNConnect.instance.CurrentJoinedRoom.Players)
        {
            var view = Pooly.Spawn(_scoreViewPrefab.transform, Vector3.zero, Quaternion.identity, _rectGameInfo);
            var scoreView = view.GetComponent<ScoreView>();
            scoreView.Initialize(player.Value.NickName);
            _dictScoreView.Add(player.Value.ActorNumber, scoreView);
        }
    }

    public void UpdatePlayerScore()
    {
        foreach (var player in PUNConnect.instance.CurrentJoinedRoom.Players)
        {
            _dictScoreView.TryGetValue(player.Value.ActorNumber, out var scoreView);
            if (scoreView != null)
            {
                var score = player.Value.GetScore();
                scoreView.SetScore(score);
            }
        }
    }

    public void SetPlayTime(float remainDuration)
    {
        _txtTimeLeft.text = TimeService.FormatTimeSpanShortly(remainDuration);
    }
}
