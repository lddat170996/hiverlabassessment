﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PLAYER_STATE
{
    NONE = 0,
    IDLE,
    READY,
    PLAYING_BATTLE,
    FINISH_GAME
}

public class NetworkPlayer : MonoBehaviourPunCallbacks
{
    private PhotonView pView;

    public int Score { get; private set; }
    public PLAYER_STATE State { get; private set; }

    public void SetState(PLAYER_STATE _state)
    {
        State = _state;
        switch (State)
        {
            case PLAYER_STATE.NONE:
                break;
            case PLAYER_STATE.IDLE:
                break;
            case PLAYER_STATE.READY:
                if (pView.Owner.IsMasterClient)
                {
                    GamePlayController.instance.IncreasePlayerReady();
                }
                break;
            case PLAYER_STATE.PLAYING_BATTLE:
                break;
            case PLAYER_STATE.FINISH_GAME:
                break;
            default:
                break;
        }
    }

    private void Awake()
    {
        pView = this.GetComponent<PhotonView>();
    }

    private void Start()
    {
        //pView.Owner.SetState(PLAYER_STATE.IDLE);
        Initialize();
    }

    public void Initialize()
    {
        Score = 0;
        pView.Owner.SetState(PLAYER_STATE.READY);
    }

    public void PickCoin(int numAdded)
    {
        pView.RPC("NetworkPickCoin", RpcTarget.AllBufferedViaServer, numAdded);
    }

    private void IncreaseScore(int add)
    {
        Score += add;
        Debug.Log("AddScore Call");
        pView.Owner.SetScore(Score);
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        EventSystemServiceStatic.DispatchAll(EVENT_NAME.UPDATE_PLAYER_INFO);
        this.SetState(pView.Owner.GetPlayerState());
    }

    #region NET WORK METHODS
    [PunRPC]
    public void NetworkPickCoin(int score)
    {
        Debug.Log($"NetworkPickCoin {score}");
        IncreaseScore(score);
    }

    [PunRPC]
    public void NetSetState(PLAYER_STATE _state)
    {
        this.SetState(_state);
    }

    #endregion
}
