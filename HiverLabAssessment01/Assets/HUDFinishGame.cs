﻿using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HUDFinishGame : BaseHUD
{
    public Button btnReplay;
    public Button btnReady;
    public PlayerResultView _playerResultView;
    public RectTransform _rectResult;
    public TextMeshProUGUI _txtWinnerIs;

    private Dictionary<int, PlayerResultView> _dictResultView = new Dictionary<int, PlayerResultView>();
    private List<Player> _listSortedPlayer;
    private Player winner;

    private void Start()
    {
        EventSystemServiceStatic.AddListener(this, EVENT_NAME.UPDATE_PLAYER_INFO, new Action(UpdatePlayerInfo));
    }

    public override void PreInit(EnumHUD type, IParentHud _parent, params object[] args)
    {
        base.PreInit(type, _parent, args);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        btnReplay.gameObject.SetActiveIfNot(PUNConnect.instance.IsMasterClient);
        btnReady.gameObject.SetActiveIfNot(!PUNConnect.instance.IsMasterClient);

        foreach (var item in _dictResultView)
        {
            GameObject.Destroy(item.Value.gameObject);
        }
        _dictResultView.Clear();

        _listSortedPlayer = new List<Player>();
        var dictPlayer = PUNConnect.instance.CurrentJoinedRoom.Players;
        winner = GamePlayController.instance.GetWinner();
        if (winner != null)
        {
            _txtWinnerIs.text = $"The Winner is {winner.NickName}";
        }


        foreach (var p in dictPlayer)
        {
            _listSortedPlayer.Add(p.Value);
        }
        _listSortedPlayer.Sort((x, y) => x.GetScore().CompareTo(y.GetScore()));
        foreach (var player in _listSortedPlayer)
        {
            var view = GameObject.Instantiate(_playerResultView, Vector3.zero, Quaternion.identity, _rectResult);
            view.Initialize(player.NickName, player.GetScore());
            _dictResultView.Add(player.ActorNumber, view);
        }

        UpdatePlayerInfo();

    }

    private void OnDestroy()
    {
        EventSystemServiceStatic.RemoveListener(this, EVENT_NAME.UPDATE_PLAYER_INFO, new Action(UpdatePlayerInfo));

    }

    private void UpdatePlayerInfo()
    {
        foreach (var player in PUNConnect.instance.CurrentJoinedRoom.Players)
        {
            bool isReady = player.Value.GetPlayerState() == PLAYER_STATE.READY;
            _dictResultView[player.Value.ActorNumber].SetPlayerReady(isReady);
        }
    }

    public void OnButtonReplay()
    {
        if (PUNConnect.instance.IsMasterClient)
        {
            foreach (var player in PUNConnect.instance.CurrentJoinedRoom.Players)
            {
                bool isReady = player.Value.GetPlayerState() == PLAYER_STATE.READY;
                if (!isReady)
                    return;
            }
            GamePlayController.instance.pView.RPC("NetReplay", Photon.Pun.RpcTarget.AllBufferedViaServer);
        }
    }

    public void OnButtonReady()
    {
        PUNConnect.instance.LocalPlayer.SetState(PLAYER_STATE.READY);
        GamePlayController.instance.pView.RPC("NetRefreshHUDFinish", Photon.Pun.RpcTarget.AllBufferedViaServer);
    }
}
