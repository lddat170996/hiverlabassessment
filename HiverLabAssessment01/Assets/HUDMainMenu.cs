﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HUDMainMenu : BaseHUD
{
    public TMP_InputField _inputField;
    
    public override void PreInit(EnumHUD type, IParentHud _parent, params object[] args)
    {
        base.PreInit(type, _parent, args);
        if (!isInit)
        {
            PUNConnect.instance.HandleJoinedRoom += OnJoinedRoom;
            isInit = true;
        }
    }

    private void OnDestroy()
    {

        PUNConnect.instance.HandleJoinedRoom -= OnJoinedRoom;
    }

    private void OnJoinedRoom(bool success)
    {
        if (success)
        {
            MainMenuCanvas.instance.ShowHUD(EnumHUD.HUD_ROOM_WAITING);
        }
    }

    public void OnButtonQuickPlay()
    {
        var name = this._inputField.text;
        if(!string.IsNullOrEmpty(name))
        {
            PUNConnect.instance.SetNickName(name.Trim());
            PUNConnect.instance.JoinRandomRoom();
        }
    }


}
