﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[Serializable]
public class HUDRef
{
    public EnumHUD type;
    public BaseHUD prefab;
}

[CreateAssetMenu(fileName = "ResourcesSO", menuName = "ScriptableObjects/ResourcesSO", order = 1)]
public class ResourcesSO : ScriptableObject
{
    [Header("HUD references")]
    public List<HUDRef> _listHUD;

    public BaseHUD GetHUD(EnumHUD type)
    {
        var refe = _listHUD.FirstOrDefault(x => x.type == type);
        if(refe != null)
        {
            return refe.prefab;
        }
        return null;
    }
}
