﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;


public static class PlayerExt
{
    #region Score

    public static void SetScore(this Player player, int amount)
    {
        Hashtable score = new Hashtable();
        score[PlayerConst.SCORE] = amount;
        player.SetCustomProperties(score);

    }

    public static int GetScore(this Player player)
    {
        object score = null;
        player.CustomProperties.TryGetValue(PlayerConst.SCORE, out score);
        if (score != null)
            return (int)score;
        else
            return 0;
    }

    #endregion

    #region State
    public static void SetState(this Player player, PLAYER_STATE _state)
    {
        Hashtable state = new Hashtable();
        state[PlayerConst.PLAYER_STATE] = _state;
        player.SetCustomProperties(state);
    }

    public static PLAYER_STATE GetPlayerState(this Player player)
    {
        object state = null;
        player.CustomProperties.TryGetValue(PlayerConst.PLAYER_STATE, out state);
        if (state != null)
            return (PLAYER_STATE)state;
        else
            return PLAYER_STATE.NONE;
    }
    #endregion
}
