﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Doozy.Engine.UI;
using System;
using Doozy.Engine.Extensions;
using Ez.Pooly;
using MEC;

public enum EnumHUD
{
    NONE,
    HUD_GAME_LOADING,
    HUD_LOADING,
    HUD_MAIN_MENU,
    HUD_ROOM_WAITING,
    HUD_SETTING,
    HUD_WINLOSE
}

public class BaseHUD : MonoBehaviour
{
    protected CanvasGroup _canvasGroup;
    protected Canvas _canvas;
    protected UIView _uiView;

    public EnumHUD _hudType;
    protected IParentHud _parentHUD;

    protected bool _isAddedStack = false;
    protected Action<bool> ShowCallback;
    protected Action<bool> HideCallback;

    public static readonly float TIME_FX = .2f;
    protected bool refreshLastLayer = true;
    protected bool isInit = false;

    public virtual void Awake()
    {
        _canvasGroup = this.GetComponent<CanvasGroup>();
        _canvas = this.GetComponent<Canvas>();
        _uiView = this.GetComponent<UIView>();
    }

    public virtual void PreInit(EnumHUD type, IParentHud _parent, params object[] args)
    {
        _parentHUD = _parent;
        _hudType = type;

        if (_uiView)
        {
            _uiView.OnVisibilityChanged.AddListener(
                new UnityEngine.Events.UnityAction<float>(OnUIViewVisibilityProgress));
        }

        ResetLayers();
    }

    protected void OnUIViewVisibilityProgress(float progress)
    {
        if (progress >= 1)
        {
            ShowCallback?.Invoke(true);
        }
        else if (progress <= 0)
        {
            HideCallback?.Invoke(true);
        }
    }

    public virtual void Init()
    {
        isInit = true;
    }

    public virtual void Show(Action<bool> showComplete = null, bool addStack = true)
    {
        ShowCallback = showComplete;
        gameObject.SetActive(true);
        if (_uiView)
        {
            _uiView.Hide(true);
            _uiView.Show(false);
        }
        else
        {
            _canvasGroup.alpha = 0;
            _canvasGroup.DOFade(1, TIME_FX).SetUpdate(true).OnComplete(() => { ShowCallback?.Invoke(true); });
        }

        _isAddedStack = addStack;
        //if (_isAddedStack)
        //    _parentHUD.MenuStack.AddMenu(_hudType);

        this.GetComponent<RectTransform>().FullScreen(true);
        
    }
    
    protected virtual List<string> GetPreInitAsset()
    {
        return null;
    }

    public virtual void CleanUp()
    {
        // Pooly.Despawn(transform);
    }

    public virtual void Hide(Action<bool> hideComplete = null)
    {
        HideCallback = hideComplete;
        if (_uiView)
        {
            _uiView.Hide(false);
        }
        else
        {
            _canvasGroup.alpha = 1;
            _canvasGroup.DOFade(0, TIME_FX).SetUpdate(true).OnComplete(() =>
            {
                gameObject.SetActive(false);
                HideCallback?.Invoke(true);
            });
        }
    }

    public virtual void HideInstantly()
    {
        _uiView.Hide(true);
    }

    public virtual void OnButtonBack()
    {
        Hide();
    }

    private void OnDestroy()
    {
        if (_uiView)
        {
            _uiView.OnVisibilityChanged.RemoveAllListeners();
        }
    }

    public virtual void ResetLayers()
    {
    }
}