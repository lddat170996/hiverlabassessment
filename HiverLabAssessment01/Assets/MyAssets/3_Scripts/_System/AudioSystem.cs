﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BGM
{
    NONE,
    BGM_MENU,
    BGM_BATTLE
};

public enum SFX
{
    NONE,
    PICK_COIN,
    BTN_CLICK
}


public class AudioSystem : BaseSystem
{
    public static AudioSystem instance;

    private void Awake()
    {
        instance = this;
    }

    public AudioSource _bgmAudio;
    public AudioSource _sfxAudio;

    public override IEnumerator<float> InitializeAsyncCoroutine(Action callback, params object[] args)
    {
        yield break;
    }

    public void PlayBGM(BGM enumBGM)
    {
        var clip = ResourceManager.instance.GetBGM(enumBGM);
        if (clip != null)
        {
            _bgmAudio.clip = clip;
            _bgmAudio.Play();
            _bgmAudio.loop = true;

        }

    }

    public void PlaySFX(SFX sfx)
    {
        var clip = ResourceManager.instance.GetSFX(sfx);
        if (clip != null)
        {
            _bgmAudio.PlayOneShot(clip);

        }
    }
}
