﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ez.Pooly;
using System;
using DG.Tweening;
using UnityEngine.UI;
using MEC;

public interface IParentHud
{
    void Initialize();
    void ShowHUD(EnumHUD _type, Action<bool> ShowComplete = null, params object[] args);
    BaseHUD HideHUD(EnumHUD _type, Action<bool> HideComplete = null);
}

public class BaseParentHUD : MonoBehaviour, IParentHud
{
    public Transform holder;
    public Transform _floatingTextTrf;
    public Canvas canvas { get; private set; }
   
    protected Dictionary<EnumHUD, BaseHUD> _dictHUD;
    private BaseHUD currentHUD;
    public BaseHUD CurrentHUD => currentHUD;
    protected Action<EnumHUD> onShowHUD;
    protected Action<EnumHUD> onResetLayer;

    
    public virtual void Initialize()
    {
        canvas = GetComponent<Canvas>();
        _dictHUD = new Dictionary<EnumHUD, BaseHUD>();
    }

    public virtual void ShowHUD(EnumHUD _type, Action<bool> ShowComplete = null, params object[] args)
    {
        BaseHUD _hud = null;
        _dictHUD.TryGetValue(_type, out _hud);
        if (_hud == null)
        {
            BaseHUD toSpawn = ResourceManager.instance._resourcesAsset.GetHUD(_type);
            if (toSpawn == null)
            {
                Debug.LogError($"cant spawn {_type}");
                return;
            }
            _hud = Pooly.Spawn(toSpawn.transform, Vector3.zero, Quaternion.identity).GetComponent<BaseHUD>();
            _hud.transform.SetParent(transform);
            _hud.transform.localPosition = Vector3.zero;
            _hud.transform.localScale = Vector3.one;

            _dictHUD.Add(_type, _hud);
        }

        _hud.transform.SetAsLastSibling();
        _hud.PreInit(_type, this, args);
        _hud.Show(ShowComplete);
        currentHUD = _hud;
        onShowHUD?.Invoke(currentHUD._hudType);

    }

    public virtual BaseHUD HideHUD(EnumHUD _type, Action<bool> HideComplete = null)
    {
        BaseHUD _hud = null;
        _dictHUD.TryGetValue(_type, out _hud);
        if (_hud)
        {
            _hud.Hide(HideComplete);
        }

        return _hud;
    }

    public virtual BaseHUD GetHUD(EnumHUD _type)
    {
        BaseHUD result = null;
        _dictHUD.TryGetValue(_type, out result);

        return result;
    }

}