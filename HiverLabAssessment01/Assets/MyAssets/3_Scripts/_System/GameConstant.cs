﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameConstant
{
    public static string LOADING_SCENE = "LoadingScene";
    public static string MENU_SCENE = "MainMenu";
    public static string GAME_SCENE_01 = "GameScene_01";
    public static string PLAYER_CONTROLLER = "PlayerController";

    public static int MINIMUM_START_GAME = 1;
    public static long GAME_DURATION = 120;   
}

public static class PlayerConst
{
    public static string SCORE = "Score";
    public static string PLAYER_STATE = "PlayerState";
}

public static class EVENT_NAME
{
    public static string UPDATE_PLAYER_INFO = "UPDATE_PLAYER_SCORE";
}


public static class TagConstant
{
    public static string TAG_COIN = "coin";
}
