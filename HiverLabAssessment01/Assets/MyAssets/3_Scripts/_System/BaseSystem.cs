﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using System;

public interface IBaseSystem
{
    bool IsInited { get; set; }
    void InitializeAsync(Action callback, params object[] args);
    void UpdateSystem(float _deltaTime);
    void FixedUpdateSystem(float _fixedDeltaTime);
}

public abstract class BaseSystem : MonoBehaviour, IBaseSystem
{
    [Header("Config")]
    public bool InitWhenStart;

    protected bool _isInited;

    public bool IsInited { get => _isInited; set => _isInited = value; }

    public virtual void InitializeAsync(Action callback, params object[] args)
    {
        Timing.RunCoroutine(InitializeAsyncCoroutine(callback, args));
    }

    public abstract IEnumerator<float> InitializeAsyncCoroutine(Action callback, params object[] args);

    public virtual void UpdateSystem(float _deltaTime)
    {
        if (!IsInited)
        {
            Debug.LogError($"{this.name} is not inited");
            return;
        }
    }

    public virtual void FixedUpdateSystem(float _fixedDeltaTime)
    {
        if (!IsInited)
        {
            Debug.LogError($"{this.name} is not inited");
            return;
        }
    }

}
