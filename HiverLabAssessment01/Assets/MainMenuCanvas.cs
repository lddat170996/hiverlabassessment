﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCanvas : BaseParentHUD
{
    public static MainMenuCanvas instance;

    public HUDMainMenu _hudMainMenu;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        this.Initialize();
        _hudMainMenu.PreInit(EnumHUD.HUD_MAIN_MENU, null);
    }
}
