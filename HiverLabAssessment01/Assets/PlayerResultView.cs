﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerResultView : MonoBehaviour
{
    public TextMeshProUGUI _txtName;
    public TextMeshProUGUI _txtScore;
    public Toggle _toggleReady;

    public void Initialize(string name, int score)
    {
        _txtName.text = name.Trim();
        _txtScore.text = score.ToString();
        _toggleReady.gameObject.SetActiveIfNot(true);
        _toggleReady.interactable = false;
        Canvas.ForceUpdateCanvases();
    }

    public void SetPlayerReady(bool _ready)
    {
        Debug.Log($"SetPlayerReady {_ready}");
        _toggleReady.isOn = _ready;
    }

}
