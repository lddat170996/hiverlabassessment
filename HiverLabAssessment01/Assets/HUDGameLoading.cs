﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDGameLoading : BaseHUD
{
    public Slider _loadingBar;

    public override void PreInit(EnumHUD type, IParentHud _parent, params object[] args)
    {
        base.PreInit(type, _parent, args);
        _loadingBar.interactable = false;
        _loadingBar.value = 0;
        _loadingBar.maxValue = 100;
    }

    public void SetLoadingProgress(float _value)
    {
        _loadingBar.value = _value; 
    }
}
